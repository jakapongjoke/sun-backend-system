const bcrypt = require('bcrypt');
var randomstring = require("randomstring");

const password = {
    encode(password){
        return bcrypt.hash(password, 10);
    },
     async compare(password,hashPassword){
        const result = await bcrypt.compare(password, hashPassword)
        return result;
    },
    async  genarate(n){
        let salt = await bcrypt.genSaltSync(10);
        let hash = await bcrypt.hashSync(randomstring.generate(n), salt);
        return hash;
    }
}
module.exports = password;