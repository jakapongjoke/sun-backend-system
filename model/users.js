module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define(
      'users',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          primaryKey: true
        },
        username: {
          type: Sequelize.STRING,
          field: 'username'
        },
        password: {
          type: Sequelize.STRING,
          field: 'password'
        },
        firstname: {
          type: Sequelize.STRING,
          field: 'firstname'
        },
        lastname: {
          type: Sequelize.STRING,
          field: 'lastname'
        },
        role: {
          type: Sequelize.STRING,
          field: 'role'
        },
        status: {
          type: Sequelize.ENUM('admin', 'user', 'operator', 'accounting'),
          field: 'role'
        },
        status: {
            type: Sequelize.ENUM('active', 'inactive', 'deleted'),
            field: 'role'
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at'
        },
        updated_at: {
            type: Sequelize.DATE,
            field: 'updated_at'
        },
        deleted_at: {
            type: Sequelize.DATE,
            field: 'deleted_at'
        }
      },
      {
        timestamps: false,
        freezeTableName: true
      }
    );
    return User;
  };