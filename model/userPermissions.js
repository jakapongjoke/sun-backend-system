module.exports = (sequelize, Sequelize) => {
    const UserPermissions = sequelize.define(
      'user_permissions',
      {
        id: {
          type: Sequelize.INTEGER,
          field: 'id',
          primaryKey: true
        },
        user_id: {
          type: Sequelize.STRING,
          field: 'user_id'
        },
        permission_data: {
          type: Sequelize.STRING,
          field: 'json'
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at'
        },
        updated_at: {
            type: Sequelize.DATE,
            field: 'updated_at'
        }
      },
      {
        timestamps: false,
        freezeTableName: true
      }
    );
    return UserPermissions;
  };