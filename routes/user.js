// import lib
const express = require('express')
const db = require('../util/db.config')
const User = require('../services/users/userService')
const UserPermissions = require('../services/users/UserPermissionService')
// define variable
const sequelize = db.sequelize;
const route = express.Router()

route.get('/get/permission/:user_id', async (req, res, next) => {
  const permission =  await UserPermission.getPermission(req.query.user_id)
    res.json(permission)
})

route.get('/find/id/:name', async (req, res, next) => {
  const user =  await User.selectAll()
    res.json(user)
})

route.post('/login', async (req, res, next) => {
  
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3333');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'HTTP_ACCESS_CONTROL_REQUEST_HEADERS');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
  const login =  await User.login(req.body.username,req.body.password)
  if(login.status==="success"){
    res.send({
      status: 'loged',
      token: login.token,
    });
  }
})
module.exports = route;
