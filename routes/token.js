// import lib
const express = require('express')
const jwt  = require('jsonwebtoken');
const db = require('../util/db.config')
const User = require('../services/users/userService')
// define variable
const sequelize = db.sequelize;
const route = express.Router()
require('dotenv').config()

route.get('/checktoken', async (req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3333');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'HTTP_ACCESS_CONTROL_REQUEST_HEADERS');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    
  var token = req.headers.authorization
console.log(req.headers.authorization)
  if (token && token.startsWith('Bearer ')) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }
  if(token) {
    jwt.verify(token, process.env.SECRET_KEY , function(err, decode){
      if(err){
        res.status(401)
        res.send({
          error: err
        })
      }else{
        res.status(200)
        res.send({
          status: 'complete'
        })
      }
   
    })
  } else {
    res.status(401)
    res.send({status: "can't access"})
  }
  

})
module.exports = route;
