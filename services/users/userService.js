// Import Config And init Sequelize
const db = require('../../util/db.config');
const bcrypt  = require('../../util/crypto/bcrypt');
const jwt  = require('jsonwebtoken');
// define variable
const sequelize = db.sequelize;
const User = db.users;
const Users = {};

Users.selectAll = async ()=>{
    const user = await User.findAll();
    return user; 
}

Users.getUser = async(username)=>{
    const user = await User.findOne({ where: {username: username }, raw : true});
    return [user];
}

Users.checkPassword = async(password,hashpassword)=>{
     const result =  await bcrypt.compare(password, hashpassword);
     return result;
}
Users.genpassword = async(password)=>{
    const hashpassword = await bcrypt.encode(password)
    return hashpassword;
}

Users.login =async (username,password)=> {
    const getUser= await Users.getUser(username);
   if(getUser.length===1){
        const checkPassword = await Users.checkPassword(password,getUser[0].password)
        if(checkPassword){
            let payloadData = {
                userId: getUser[0].id.toString(),
                username:getUser[0].name,
                role:getUser[0].role,
            }
            const token = await jwt.sign(payloadData,process.env.SECRET_KEY,{ expiresIn: '1h' });
            const result = {
                status : 'success',
                token,
            }

            return result;
        }else{
            const result = {
                status : 'wrong password',
                token : token
            }
            return result;
        }

   }else{
       
   }
}

 
module.exports = Users;
