// Import Config And init Sequelize
const db = require('../../util/db.config');
const bcrypt  = require('../../util/crypto/bcrypt');
const jwt  = require('jsonwebtoken');
// define variable
const sequelize = db.sequelize;
const UserPermissions = db.UserPermissions;
const UsersPermission = {};

//Get a user permission
UsersPermission.getPermission = async(userid)=>{
    const user = await UserPermissions.findOne({ where: {user_id: userid }, raw : true});
    return [user];
}

module.exports = UsersPermission;