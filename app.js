const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
app.use(cors());

// set use body json
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// add route
const userRoute = require('./routes/user');
const tokenRoute = require('./routes/token');
app.use('/v1/user', userRoute);
app.use('/v1/token', tokenRoute);
// set port & run server
app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// ทดสอบ